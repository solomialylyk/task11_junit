package solomia;

import solomia.model.Minesweeper;
import solomia.model.Plateau;
import solomia.view.MyView;

import java.lang.reflect.Field;

public class Application {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Minesweeper minesweeper= new Minesweeper();
        Field field=Minesweeper.class.getDeclaredField("N");
        field.setAccessible(true);
        int fieldValue= (int) field.get(minesweeper);
        System.out.println(fieldValue);
       // new MyView().show();


    }
}
