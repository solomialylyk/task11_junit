package solomia.controller;

public interface Controller {
    int [] findLongest();
    void printGame();
    void play ();
}
