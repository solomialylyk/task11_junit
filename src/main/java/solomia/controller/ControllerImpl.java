package solomia.controller;

import solomia.model.Minesweeper;
import solomia.model.Plateau;

public class ControllerImpl implements Controller {
    Plateau plateau= new Plateau();
    Minesweeper minesweeper = new Minesweeper();

    @Override
    public int [] findLongest() {
        return  plateau.findLongest();

    }
    @Override
    public void printGame() {
        minesweeper.printGame();
    }

    @Override
    public void play() {
        minesweeper.play();
    }
}
