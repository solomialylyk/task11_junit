package solomia.model;

import java.util.Arrays;

public class Plateau {
    private int []mas={1,1,1,2,2,2,2,1,1,4,4,5};

    public Plateau() {

    }

    public Plateau(int[] mas) {
        this.mas = mas;
    }


    public int[] getMas() {
        return mas;
    }

    public void setMas() {
        this.mas = mas;
    }

    public int [] findLongest(){
        int countGlobal=0;
        int endGlobal=0;
        int beginGlobal=0;
        for (int i=0; i<mas.length; ++i) {
            int count=0;
            int end=0;
            for (int j=i; j<mas.length; j++) {
                if(mas[i]==mas[j]) {
                    end=j;
                    count++;
                }
                else {
                    break;
                }
            }
            if(countGlobal<count) {
                countGlobal=count;
                beginGlobal=i;
                endGlobal=end;

            }
        }
        System.out.println("count "+ countGlobal);
        System.out.println("Begin index " + beginGlobal);
        System.out.println("End index " + endGlobal);
//        System.out.println("Finded plateau: ");
//        for (int i=beginGlobal; i<=endGlobal; i++) {
//            System.out.print(mas[i] + " ");
//        }
        int mas1[]=new int[countGlobal+1];
        int i=0;
        for (int j=beginGlobal; j<=endGlobal; j++) {
            mas1[i]=mas[j];
            i++;
        }

        return mas1;
    }
    public int[] r() {

        return mas;
    }

    @Override
    public String toString() {
        return "Plateau{" +
                "mas=" + Arrays.toString(mas) +
                '}';
    }
}
