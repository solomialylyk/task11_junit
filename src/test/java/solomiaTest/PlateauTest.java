package solomiaTest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import solomia.model.Plateau;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PlateauTest {
    @BeforeEach
    void before() {
        System.out.println("Before each test");
    }

    @RepeatedTest(3)
    void checkPlateauResult(){
        Plateau plateau= new Plateau();

        int mas[]=plateau.findLongest();
        int []checkResult={2,2,2,2};
        for(Integer i: mas){
            assertTrue(mas[i]==checkResult[i]);
        }
    }

    @Test
    void checkSize() {
        Plateau plateau= new Plateau();

        int mas[]=plateau.findLongest();
        int []checkResult={2,2,2,2};
        assertNotEquals(mas.length, checkResult.length);
    }


    @AfterEach
    void after() {
        System.out.println('\n'+"After each test");
    }
}
