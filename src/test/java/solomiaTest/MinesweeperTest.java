package solomiaTest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import solomia.model.Minesweeper;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MinesweeperTest {
    @BeforeEach
    void before() {
        System.out.println("Before each test");
    }

    @Test
    void checkM() throws NoSuchFieldException, IllegalAccessException {
solomia.model.Minesweeper minesweeper= new solomia.model.Minesweeper();
        Field field= solomia.model.Minesweeper.class.getDeclaredField("N");
        field.setAccessible(true);
        int fieldValue= (int) field.get(minesweeper);
        System.out.println(fieldValue);
        assertTrue(fieldValue==3);
    }

    @AfterEach
    void after() {
        System.out.println("After each test");
    }

    private class Minesweeper {
    }
}
